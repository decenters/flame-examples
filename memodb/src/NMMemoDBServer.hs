{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE PostfixOperators #-}
{-# OPTIONS_GHC -fplugin Flame.Solver #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

-- Based on Servant.API.Experimental.Auth and
-- https://github.com/krdlab/examples/blob/master/haskell-servant-webapp/src/Main.hs
module NMMemoDBServer where

import Prelude hiding (id)

import GHC.TypeLits (KnownNat, KnownSymbol, natVal, symbolVal)
import Network.HTTP.Types.Status
import Network.HTTP.Types.Method
import Network.HTTP.Types.Header

import qualified Control.Monad.IO.Class as XXX (liftIO) 
import Data.Aeson (FromJSON(..), ToJSON(..))
import qualified Data.Map as M
import Data.Text (Text)
import Data.Time (LocalTime(..), TimeZone(..), ZonedTime(..), hoursToTimeZone, midday, fromGregorian, utcToZonedTime, getCurrentTimeZone)
import GHC.Generics (Generic)
import Network.Wai (Application, Response)
import Network.Wai.Handler.Warp (run)
import System.IO as SIO
import Servant ((:>), (:<|>)(..), ReqBody, Capture, Get, Post, Delete, Proxy(..), Server, serve, ServantErr, HasServer(..), reflectMethod, ReflectMethod(..), serveDirectoryFileServer)
import           Servant.API.ContentTypes
import Servant.Docs (docs, ToCapture(..), DocCapture(..), ToSample(..), markdown, singleSample, HasDocs(..))
import Servant.Docs.Internal (ToAuthInfo(..), authInfo, DocAuthentication(..))
import Servant.Server.Internal
import Control.Lens ((|>), over)
import System.Environment (getArgs)

import Data.Maybe  (fromJust, fromMaybe)
import Data.Aeson                       (ToJSON)
import Data.ByteString                  (ByteString)
import Data.Map                         (Map, fromList)
import Data.Monoid                      ((<>))
import qualified Data.Map            as Map
import Data.Proxy                       (Proxy (Proxy))
import Data.Text                        (Text, pack)
import GHC.Generics                     (Generic)
import Network.Wai                      (Request, requestHeaders)
import Servant.API                      ((:<|>) ((:<|>)), (:>), BasicAuth,
                                          Get, JSON, Verb(..))
import Servant.API.BasicAuth            (BasicAuthData (BasicAuthData))
import Servant.API.Experimental.Auth    (AuthProtect)
import Servant                          (throwError)
import Servant.Server                   (BasicAuthCheck (BasicAuthCheck),
                                         BasicAuthResult( Authorized
                                                        , Unauthorized
                                                        ),
                                         Context ((:.), EmptyContext),
                                         err401, err403, errBody, Server,
                                         serveWithContext, Handler,
                                         ServerT, (:~>)(..), Application, serve, enter)
import Servant.Server.Experimental.Auth (AuthHandler, AuthServerData)
import Servant.Server.Experimental.Auth()
import Control.Monad.Trans.Resource     (MonadResource (..), ResourceT, runResourceT)


import Flame.Servant.Server
import Flame.Servant.BasicAuth

import Flame.Runtime.Principals
import Flame.IFC
import Flame.Principals
import Flame.Runtime.Time as T
import Control.Concurrent.STM hiding (atomically)
import Flame.Runtime.STM as F
import qualified Flame.Runtime.Prelude as F hiding (id)
import Flame.Runtime.Sealed
import Flame.Runtime.IO
import Data.String
import System.IO.Unsafe
import Control.Monad.Trans.Either (EitherT)

import Flame.TCB.Assume
import Flame.TCB.IFC (Lbl(..))

-- JS stuff
import qualified Data.Text.IO as TextIO
import Servant.Foreign
import Servant.JS
import qualified Language.Javascript.JQuery
import Control.Lens hiding (use, Context)
import Data.Text.Encoding

import Flame.Assert 

import Debug.Trace

type MemoServer = KTop
type MemoClient = KName "MemoClient"

{- Auth specific -}
type Alice = KName "Alice"
alice = Name "Alice"

type Bob = KName "Bob"
bob = Name "Bob"

type Carol = KName "Carol"
carol = Name "Carol"

instance IFCApp (BasicAuthNM "memo-server" Prin) where
  type AppServer (BasicAuthNM "memo-server" Prin) = MemoServer
  type Client (BasicAuthNM "memo-server" Prin)    = MemoClient
  type AuthBound (BasicAuthNM "memo-server" Prin) = I MemoServer
  
  appServerPrin = const $ Top
  appServerTy   = const $ STop

  currentClientPrin = const $ Name "MemoClient"
  currentClientTy   = const $ SName (Proxy :: Proxy "MemoClient")

  authBoundPrin api = (Integ $ appServerPrin api)
  authBoundTy   api = ((appServerTy api)*<-)

type BasicAPI = (BasicAuthNM "memo-server" Prin)
basicAPI = Proxy :: Proxy BasicAPI
cli = currentClientTy basicAPI

{- common defs -}
-- | 'BasicAuthCheck' holds the handler we'll use to verify a username and password.
authCheck :: NMAuthCheck BasicAPI Prin
authCheck lauth =
  let lauth' = endorse $ lift lauth :: NM IO (I MemoServer) (I MemoServer ∧ C MemoClient) BasicAuthData
      res    = (use lauth' $ \(BasicAuthData username password) ->
                ebind user_database $ \database ->
                 case Map.lookup username database of
                   Nothing  -> trace "no user" $ protect Unauthorized
                   Just pwd -> if password == pwd then
                                 trace "authorized" $ protect (Authorized (Name $ decodeUtf8 username))
                               else
                                 trace "unauthorized" $ protect Unauthorized) :: NM IO (I MemoServer) MemoServer (BasicAuthResult Prin)
  in declassify res

-- | We need to supply our handlers with the right Context. In this case,
-- Basic Authentication requires a Context Entry with the 'BasicAuthCheck' value
-- tagged with "foo-tag" This context is then supplied to 'server' and threaded
-- to the BasicAuth HasServer handlers.
basicAuthServerContext :: Context (NMAuthCheck (BasicAuthNM "memo-server" Prin) Prin ': '[])
basicAuthServerContext = authCheck :. EmptyContext

-- | A password database
user_database :: Lbl MemoServer (Map ByteString ByteString)
user_database = label $ fromList [ ("alice", "12345")
                                   , ("bob", "secret")
                                   , ("carol", "password")
                                   ]

{- memo specific -}
data Memo = Memo
    { id :: Int
    , text :: Text
    , createdAt :: UTCTime
    }
    deriving (Show, Generic)

instance ToJSON Memo

data ReqMemo = ReqMemo { memo :: Text } deriving (Show, Generic)

instance FromJSON ReqMemo
instance ToJSON ReqMemo

type MemoAPI =
         "memos" :> BasicAuthNM "memo-server" Prin
          :> EnforceNM (I MemoClient) (MemoClient) (Get '[JSON] [Memo]) 
    :<|> "memos" :> BasicAuthNM "memo-server" Prin
          :> ReqBody '[JSON] ReqMemo
          :> EnforceNM MemoClient MemoClient (Post '[JSON] Memo) 
    :<|> "memos" :> BasicAuthNM "memo-server" Prin
         :> Capture "id" Int
         :>  EnforceNM (I MemoClient) (MemoClient) (Delete '[JSON] ())

memoAPI :: Proxy MemoAPI
memoAPI = Proxy

type MemoAPI_Raw = MemoAPI :<|> Raw
memoAPI_Raw :: Proxy MemoAPI_Raw
memoAPI_Raw = Proxy

type MemoAuth = IFCAuth MemoAPI

type MemoStore = Lbl (I MemoServer) MemoDB
--type MemoDB = IFCTVar (I MemoServer) (M.Map Prin (SealedType IFCTVar (Int, M.Map Int Memo)))
-- TODO: refactor to shared Memo server
type MemoDB = IFCTVar MemoClient (Int, M.Map Int Memo)

-- | The context that will be made available to request handlers. We supply the
-- "cookie-auth"-tagged request handler defined above, so that the 'HasServer' instance
-- of 'AuthProtect' can extract the handler and run it on the request.
genAuthServerContext :: Context (NMAuthCheck BasicAPI Prin ': '[])
genAuthServerContext = authCheck :. EmptyContext

apiJS :: Text
apiJS = jsForAPI memoAPI jquery

instance (HasForeignType lang ftype Text, HasForeign lang ftype api)
  => HasForeign lang ftype (BasicAuthNM "memo-server" Prin :> api) where
  type Foreign ftype (BasicAuthNM "memo-server" Prin :> api) = Foreign ftype api

  foreignFor lang Proxy Proxy req =
    foreignFor lang Proxy subP $ req & reqHeaders <>~ []
    where
      hname = "memo-server-principal" -- probably unecessary
      arg   = Arg
        { _argName = PathSegment hname
        , _argType  = typeFor lang (Proxy :: Proxy ftype) (Proxy :: Proxy Text) }
      subP  = Proxy :: Proxy api

writeJS :: IO ()
writeJS = do
  TextIO.writeFile "nm/html/api.js" apiJS
  jq <- TextIO.readFile =<< Language.Javascript.JQuery.file
  TextIO.writeFile "nm/html/jq.js" jq

main :: IO ()
main = writeJS >> runApp

runApp :: IO ()
runApp = do 
  init <- runNM $ newIFCTVarIO (0, M.empty)
  run 8080 $ app init

app :: MemoStore -> Application
app s = serveWithContext memoAPI_Raw genAuthServerContext $ (server s :<|> serveDirectoryFileServer "nm/html")

{- common defs -}
instance IFCApp MemoAPI where
  type AppServer MemoAPI = MemoServer
  type Client MemoAPI = MemoClient
  type AuthBound MemoAPI = I MemoServer
  
  appServerPrin = const $ Top
  appServerTy   = const $ STop

  currentClientPrin = const $ Name "MemoClient"
  currentClientTy   = const $ SName (Proxy :: Proxy "MemoClient")

  authBoundPrin api = (Integ $ appServerPrin api)
  authBoundTy   api = ((appServerTy api)*<-)

server :: MemoStore -> Server MemoAPI
server s = getMemosH :<|> postMemoH :<|> deleteMemoH 
  where
    deleteMemoH :: Prin -> Int -> NM Handler (I MemoClient) MemoClient ()
    deleteMemoH p i = liftIO $ deleteMemo s i

    getMemosH :: Prin -> NM Handler (I MemoClient) MemoClient [Memo]
    getMemosH p = liftIO $ getMemos s

    postMemoH :: Prin -> ReqMemo -> NM Handler MemoClient MemoClient Memo
    postMemoH p req = liftIO $ postMemo s req

getMemos :: MemoStore -> NM IO (I MemoClient) MemoClient [Memo]
getMemos db =  ebind db $ \db' -> atomically $
    use (readIFCTVar @_ @_ @MemoClient @_ @(I MemoClient) db') $ \(_, m) ->
     protect $ M.elems m

postMemo :: MemoStore -> ReqMemo -> NM IO MemoClient MemoClient Memo
postMemo db req = ebind db $ \db' ->
   use getCurrentTime $ \time -> atomically $
   use (readIFCTVar db') $ \(c, sm) ->
     let m = Memo (c + 1) (memo req) time
         entry = (id m, M.insert (id m) m sm)
     in
      apply @_ @_ @_ @MemoClient @MemoClient @MemoClient
       (writeIFCTVar @_ @_ @MemoClient db' entry) $ \m' ->
       protect m

deleteMemo :: MemoStore -> Int -> NM IO (I MemoClient) MemoClient ()
deleteMemo db i = ebind db $ \db' ->
  atomically $ modifyIFCTVar db' $ \(c, sm) -> (c, M.delete i sm)

-- NOTE: ↓ for documentation
instance ToCapture (Capture "id" Int) where
    toCapture _ = DocCapture "id" "memo id"

instance ToSample Memo where
    toSamples _ = singleSample sampleMemo

instance ToSample [Memo] where
    toSamples _ = singleSample [sampleMemo]

instance ToSample ReqMemo where
    toSamples _ = singleSample $ ReqMemo "Try haskell-servant."

instance ToSample (NM IO (I MemoClient) MemoClient Memo) where
    toSamples _ = singleSample $ protect sampleMemo

instance ToSample (NM IO (I MemoClient) MemoClient [Memo]) where
    toSamples _ = singleSample $ protect [sampleMemo]

instance ToSample (NM IO (I MemoClient) MemoClient ()) where
    toSamples _ = singleSample $ protect ()

sampleMemo :: Memo
sampleMemo = Memo
    { id = 5
    , text = "Try haskell-servant."
    , createdAt = UTCTime (fromGregorian 2014 12 31) (secondsToDiffTime 0)
    }

instance (ToAuthInfo (AuthProtect "cookie-auth"), HasDocs api) => HasDocs (AuthProtect "cookie-auth" :> api) where
  docsFor Proxy (endpoint, action) =
    docsFor (Proxy :: Proxy api) (endpoint, action')
      where
        authProxy = Proxy :: Proxy (AuthProtect "cookie-auth")
        action' = over authInfo (|> toAuthInfo authProxy) action

instance ToAuthInfo (AuthProtect "cookie-auth") where
  toAuthInfo p = DocAuthentication "servant-auth-cookie" "A key in the principal database"
 
