{-# LANGUAGE TypeOperators, PostfixOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -fplugin Flame.Solver #-}

import Prelude hiding ( return, (>>=), (>>)
                      , print, putStr, putStrLn, getLine)
import qualified Prelude as P ( return, (>>=), (>>) )
import Data.List
import Data.Proxy
import Data.String
import Data.Maybe
import Flame.Runtime.Time
import Data.Time as T
import Data.ByteString.Char8 (ByteString, pack, unpack, empty)

import Flame.Principals
import Flame.Runtime.Prelude
import Flame.Runtime.Principals
import Flame.Runtime.IO
import Flame.IFC 
import qualified System.IO as SIO
import Data.IORef as SIO
import Flame.Runtime.IFCRef as Ref

import Control.Monad.Identity

{- Static principals -}
alice = SName (Proxy :: Proxy "Alice")
type Alice = KName "Alice"
aliceBid :: Lbl Alice Int
aliceBid = label 4

bob = SName (Proxy :: Proxy "Bob")
type Bob = KName "Bob"
bobBid :: Lbl Bob Int
bobBid = label 3                                          

commit :: Monad e => a -> FLAC e (I p) p a
commit = protect

receive :: Monad e => Lbl p a -> FLAC e (I q) (I q) (Lbl p a)
receive = protect

relabelXXX :: forall p q a . SPrin p -> SPrin q -> Lbl p a -> Lbl q a 
relabelXXX p q v =
  let y = runFLAC @Identity @Lbl @PT @q @a $ assume2 (p ⊑ q) $ lift (relabel @Lbl @p @q v) in
      runIdentity y

main = undefined

bad_receive :: Monad e => SPrin p -> SPrin q -> Lbl (p ∧ C q) a -> FLAC e (I q) (I q) (Lbl (p ∧ C q) a)
bad_receive p q v = protect

{- does compile 
bad_open :: Monad e => SPrin p -> SPrin q -> FLAC e ((∇) p) (p ∧ q) a -> FLAC e ((∇) p) ((I p) ∧ q) a
bad_open p q v = assume ((*∇) (q) ≽ (*∇) (p)) $ assume ((q*→) ≽ (p*→)) (reprotect v)
-}
{-
open :: Monad e => SPrin p -> SPrin q -> FLAC e ((∇) p) (p ∧ (I q)) a -> FLAC e ((∇) p) (I (p ∧ q) ∧ C (p ∨ q)) a
open p q v = assume ((*∇) (q) ≽ (*∇) (p)) $ assume ((q*→) ≽ (p*→)) (reprotect v)
-}
{- Forced to commit only public data. committing it makes it secret. -}
{- Alternative: with higher integrity pc, could declass, then endorse. -}
{-
nm_commit :: (Monad e, p ~ N p') =>
             SPrin p
             -> NM e (I p) KBot a
             -> NM e (I p) p a
nm_commit p v = let v' = endorse v in
                  reprotect v'

nm_receive :: (Monad e, p ~ N p') =>
              SPrin p
              -> SPrin q
              -> NM e (I p) p a -> NM e (I q) (I q) (NM e (I p) p a)
nm_receive p q v = protect v

{-
nm_badreceive :: (NMIF m e n, p ⊑ Δ p, I q ≽ (∇) q) =>
                 SPrin p
                 -> SPrin q
                 -> m e n (I q) (p ∧ C q) a
                 -> m e n (I q) (p ∧ q) a
nm_badreceive p q v = endorse v
-}
{-
nm_open :: (Monad e, p ~ N p') => SPrin p
           -> SPrin q
           -> NM e ((∇) p) (p ∧ (I q)) a
           -> NM e ((∇) p) (I (p ∧ q) ∧ C (p ∨ q)) a
nm_open p q v = declassify v

flacAuction :: IO (Lbl SU ())
flacAuction =
  do
    abid_rcvd <- runFLAC $ receive alice bob (lift aliceBid)
    bbid_rcvd <- runFLAC $ receive bob alice (lift bobBid)
    abid_open <- runFLAC $ open alice bob (lift abid_rcvd)
    bbid_open <- runFLAC $ open bob alice (lift bbid_rcvd)
    let winner = bind abid_open $ \abid ->
                 bind bbid_open $ \bbid ->
                 if abid > bbid then
                   label "Alice wins" :: Lbl (I (Alice ∧ Bob) ∧ C (Alice ∨ Bob)) String
                 else if abid < bbid then
                   label "Bob wins" 
                 else 
                   label "Tie" in
      runFLAC $ do
        ebind winner $ \win ->
         hPutStrLn @_ @_ @(I (Alice ∧ Bob) ∧ C (Alice ∨ Bob)) output win
  where 
   output = mkStdout (((alice *∧ bob)*←) *∧ ((alice *∨ bob)*→))
   (>>=)  = (P.>>=)

badflacAuction :: IO (Lbl SU ())
badflacAuction =
  do
    bobAttackBid <- runFLAC $ bobAttack
    abid_rcvd <- runFLAC $ bad_receive alice bob (lift $ relabel aliceBid)
    bbid_rcvd <- runFLAC $ bad_receive bob alice (lift $ bobAttackBid)
    abid_open <- runFLAC $ bad_open alice bob (lift abid_rcvd)
    bbid_open <- runFLAC $ bad_open bob alice (lift bbid_rcvd)
    let winner = bind abid_open $ \abid ->
                 bind bbid_open $ \bbid ->
                 if abid > bbid then
                   label "Alice wins" :: Lbl (Alice ∧ Bob) String
                 else if abid < bbid then
                   label "Bob wins"
                 else 
                   label "Tie" in
      runFLAC $ do ebind winner $ \win ->
                    hPutStrLn @_ @_ @(Alice ∧ Bob) output win
  where 
   pcb = (((alice *∧ bob)*←) *∧ ((alice *∧ bob)*→))
   output = mkStdout pcb 
   (>>=)  = (P.>>=)
   bobAttack :: FLAC IO (I Bob ∧ C Alice) (Bob ∧ C Alice) Int
   bobAttack = assume ((alice*←) ≽ (bob*←)) $
     ebind aliceBid $ \abid -> protect $ abid + 1

nmAuction :: IO (Lbl SU ())
nmAuction =
  do
    abid_rcvd <- runNM $ nm_receive alice bob (lift aliceBid)
    bbid_rcvd <- runNM $ nm_receive bob alice (lift bobBid)
    abid_open <- runNM $ nm_open alice bob (lift abid_rcvd)
    bbid_open <- runNM $ nm_open bob alice (lift bbid_rcvd)
    let winner = bind abid_open $ \abid ->
                 bind bbid_open $ \bbid ->
                 if abid > bbid then
                   (label "Alice wins"  :: Lbl (I (Alice ∧ Bob) ∧ C (Alice ∨ Bob)) String) 
                 else if abid < bbid then
                   label "Bob wins" 
                 else 
                   label "Tie" in
     runNM $ ebind winner $ \win -> 
              hPutStrLn @_ @_ @(I (Alice ∧ Bob) ∧ C (Alice ∨ Bob)) output win
  where 
   pcb = (((alice *∧ bob)*←) *∧ ((alice *∨ bob)*→))
   output = mkStdout pcb
   (>>=)  = (P.>>=)

main = do
     flacAuction;
     badflacAuction;
     nmAuction
 where
   (>>)  = (P.>>)
   -}
-}
