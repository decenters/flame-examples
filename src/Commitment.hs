{-# LANGUAGE TypeOperators, PostfixOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -fplugin Flame.Solver #-}
module Commitments where

import Flame.Principals
import Flame.Runtime.Principals
import Flame.IFC

commit :: FLA m e n => SPrin p -> n KBot a -> m e n (I p) p a
commit p v = assume ((SBot*←) ≽ (p*←)) (reprotect $ lift v)

receive :: FLA m e n => SPrin p -> SPrin q -> n p a -> m e n (I p ∧ I q) (p ∧ (I q)) a
receive p q v = assume (p ≽ (q*←)) (reprotect $ lift v)

{- does compile -}
bad_receive :: FLA m e n => SPrin p -> SPrin q -> n (p ∧ C q) a -> m e n (I p ∧ I q) (p ∧ q) a
bad_receive p q v = assume (p ≽ (q*←)) (reprotect $ lift v)

open :: FLA m e n => SPrin p -> SPrin q -> n (p ∧ (I q)) a -> m e n ((∇) p) ((I p) ∧ q) a
open p q v = assume ((*∇) (q) ≽ (*∇) (p)) $ assume ((q*→) ≽ (p*→)) (reprotect $ lift v)

{- Forced to commit only public data. committing it makes it secret. -}
{- Alternative: with higher integrity pc, could declass, then endorse. -}
nm_commit :: forall m e n p a. NMIF m e n => n KBot a -> m e n (I p) p a
nm_commit v = let v' = endorse $ lift v :: m e n (I p) (C KBot ∧ I p) a in
                  reprotect v'

nm_receive :: (NMIF m e n, p ≽ (∇) p) => n p a -> m e n (I p ∧ I q) (p ∧ (I q)) a
nm_receive v = endorse $ lift v

{- does not compile
nm_badreceive :: (NMIF m e n, p ≽ (∇) p) =>
                 n (p ∧ C q) a
                 -> m e n (I p ∧ I q) (p ∧ q) a
nm_badreceive v = endorse $ lift v
-}

nm_open :: forall m e n p q a. (NMIF m e n, p ⊑ Δ p) =>
           n (p ∧ (I q)) a
           -> m e n (I p ∧ I q) ((I p) ∧ q) a
nm_open v = declassify $ lift v :: m e n (I p ∧ I q) ((I p) ∧ q) a