{-# LANGUAGE TypeOperators, PostfixOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RebindableSyntax #-}
{-# OPTIONS_GHC -fplugin Flame.Type.Solver #-}

import Prelude hiding ( return, (>>=), (>>)
                      , print, putStr, putStrLn, getLine)
import qualified Prelude as P ( return, (>>=), (>>) )
import Data.List
import Data.Proxy
import Data.String
import Data.Hex
import Data.Maybe
import Flame.Time
import Data.Time as T
import Data.ByteString.Char8 (ByteString, pack, unpack, empty)

import Flame.Prelude
import Flame.Macaroons
import qualified Macaroons as M
import Flame.Data.Principals
import Flame.Type.Principals
import Flame.Type.IFC 
import Flame.Type.TCB.IFC 
import Flame.IO
import qualified System.IO as SIO
import Data.IORef as SIO
import Flame.IFCRef as Ref
import Data.Functor.Identity

runPureIFC :: IFC Identity pc l a -> Lbl l a
runPureIFC = runIdentity . runIFC
runPureIFCx :: SPrin pc -> IFC Identity pc l a -> Lbl l a
runPureIFCx pc = runPureIFC

{- Static principals -}
alice = SName (Proxy :: Proxy "Alice")
type Alice = KName "Alice"

bob = SName (Proxy :: Proxy "Bob")
type Bob = KName "Bob"

carol = SName (Proxy :: Proxy "Carol")
type Carol = KName "Carol"

{- Alice's birthday (shared with Bob) -}
birthday :: Lbl (I Alice ∧ C (Alice ∨ Bob)) Day
--birthday :: Lbl (I Alice ∧ C (Alice ∨ Bob ∨ Carol)) Day
birthday = label $ fromGregorian 2016 9 3

{- The macaroon key -}
cKey :: Lbl Carol ByteString
cKey = label "secret"

cKeyName :: ByteString
cKeyName = "key"

carolLoc = "loc://carol"

carolOutputMacaroon :: IFCHandle ((C (Bob ∨ Carol)) ∧ (I Carol))
                    -> IFC IO ((C (Bob ∨ Carol)) ∧ (I Carol)) SU ()
carolOutputMacaroon toBobFromCarol =
    assume ((*∇) bob ≽ (*∇) carol) $ 
    assume ((bob*→) ≽ (carol*→)) $ do
      mac <- liftx (carol*←) carolmac 
      let (serialized, MacaroonSuccess) = serialize mac MacaroonV1 in
        hPutStrLnx pc toBobFromCarol $ unpack serialized
  where
    pc = ((bob *∨ carol)*→) *∧ (carol*←)
    carolmac :: Lbl Carol Macaroon
    carolmac = bind cKey $ \key ->
              case create carolLoc key cKeyName of
                (m, MacaroonSuccess) -> label m
                _ -> error "Error creating macaroon"
--
checkTimeAfter :: SPrin pc -> SPrin l -> String -> IFC IO pc l Bool
checkTimeAfter pc l caveat =
  if "time >= " `isPrefixOf` caveat then
    case (parseTimeM True defaultTimeLocale "%Y-%m-%d" $ drop 7 caveat) of
      Just when -> do
        now <- getCurrentTimex pc
        return $ (utctDay now) >= when
      Nothing -> protect False
  else protect False

carolUpdateMessage :: SPrin p
                   -> IFCHandle (C (p ∨ Carol) ∧ I p)
                   -> IFCRef Carol String 
                   -> IFC IO Carol SU ()
carolUpdateMessage p from_p message =
  do (mac, err) <- inputMac
     if err /= MacaroonSuccess then do
       error "Could not deserialize macaroon."
     else do
       v <- verifierCreatex pc pc l
       satisfyExactx pc v "op: update"
       satisfyGeneralx pc v (checkTimeAfter pc l)
       (res, _) <- verifyx pc v mac cKey []
       if res then
          assume ((p*←) ≽ (carol*←)) $ do
            msg <- hGetLinex pc from_p
            writeIFCRefx pc message msg
       else 
         error "Could not verify macaroon."
 where pc = carol
       l  = carol
       inputMac :: IFC IO Carol Carol (Macaroon, ReturnCode)
       inputMac = assume ((p*←) ≽ (carol*←)) $ do
                   mac <- hGetLinex pc from_p
                   return $ deserialize . pack $ mac

bobOutputMacaroon :: IFCHandle (C (Bob ∨ Carol) ∧ I Carol)
                  -> IFCHandle (C (Alice ∨ Bob ∨ Carol) ∧ I Bob)
                  -> IFC IO (C (Alice ∨ Bob ∨ Carol) ∧ I Bob) SU ()
bobOutputMacaroon fromCarol toAlice =
  do day <- getBirthday
     --day <- getTomorrow
     (mac, err) <- inputMac
     let (mac1, MacaroonSuccess) = addFirstPartyCaveat mac (after day) 
         (mac2, MacaroonSuccess) = addFirstPartyCaveat mac1 "op: read"
         (serialized, MacaroonSuccess) = serialize mac2 MacaroonV1 in
         hPutStrLnx pc toAlice $ unpack serialized
  where after day = pack $ "time >= " ++ formatTime defaultTimeLocale "%Y-%m-%d" day
        pc = ((alice *∨ bob *∨ carol)*→) *∧ (bob*←)
        getBirthday :: IFC IO (C (Alice ∨ Bob ∨ Carol) ∧ I Bob)
                              (C (Alice ∨ Bob ∨ Carol) ∧ I Bob) Day
        getBirthday = assume ((alice*←) ≽ (bob*←)) $
                        liftx pc $ relabel birthday
        --getTomorrow :: IFC IO (C (Alice ∨ Bob ∨ Carol) ∧ I Bob)
        --                      (C (Alice ∨ Bob ∨ Carol) ∧ I Bob) Day
        --getTomorrow = do now <- getCurrentTimex pc
        --                 return $ addDays 1 (utctDay now)
        inputMac :: IFC IO (C (Alice ∨ Bob ∨ Carol) ∧ I Bob)
                           (C (Alice ∨ Bob ∨ Carol) ∧ I Bob) (Macaroon, ReturnCode)
        inputMac = assume ((carol*←) ≽ (bob*←)) $
                   assume ((*∇) (alice *∨ carol) ≽ (*∇) bob) $ 
                   assume ((alice *∨ carol) ≽ bob) $ do
                    mac <- hGetLinex bob fromCarol
                    return $ deserialize . pack $ mac

carolOutputMessage :: SPrin p
                   -> IFCHandle (C (p ∨ Carol) ∧ I p)
                   -> IFCHandle (C (p ∨ Carol) ∧ I Carol)
                   -> IFCRef Carol String 
                   -> IFC IO Carol SU ()
carolOutputMessage p from_p to_p message = 
  do (mac, err) <- inputMac
     if err /= MacaroonSuccess then do
       error "Could not deserialize macaroon."
     else do
       v <- verifierCreatex pc pc l
       satisfyExactx pc v "op: read"
       satisfyGeneralx pc v (checkTimeAfter pc l)
       (res, _) <- verifyx pc v mac cKey []
       if res then
         assume ((*∇) p ≽ (*∇) carol) $ 
          assume (p ≽ carol) $ do
           msg <- readIFCRefx carol message
           hPutStrLnx carol to_p msg
       else
         error "Cannot verify macaroon."
  where pc = carol 
        l  = carol
        inputMac :: IFC IO Carol Carol (Macaroon, ReturnCode)
        inputMac = assume ((p*←) ≽ (carol*←)) $ do
                    mac <- hGetLinex carol from_p
                    return $ deserialize . pack $ mac

main :: IO (Lbl SU ())
main = do 
  {- create a protected reference for bob's message -}
  lmsg <- runIFC $ newIFCRefx publicTrusted carol "no message"
  let message = unlabelPT lmsg in do
    {- carol sends bob a macaroon -}
    runIFC $ carolOutputMacaroon toBobFromCarol 
    {- carol sends bob a macaroon -}
    runIFC $ carolUpdateMessage bob fromBobToCarol message 
    {- bob gets macaroon from carol, creates caveats, and sends alice a macaroon -}
    runIFC $ bobOutputMacaroon fromCarolToBob toAliceFromBobForCarol
    {- bob gets macaroon from carol, creates caveats, and sends alice a macaroon -}
    runIFC $ carolOutputMessage alice fromAliceToCarol toAliceFromCarol message
 where
   {- Channel: Carol -> Bob -}
   toBobFromCarol  = mkStdout (((bob *∨ carol)*→) *∧ (carol*←))
   fromCarolToBob  = mkStdin  (((bob *∨ carol)*→) *∧ (carol*←))
   {- Channel: Bob -> Carol -}
   toCarolFromBob  = mkStdout  (((bob *∨ carol)*→) *∧ (bob*←))
   fromBobToCarol  = mkStdin  (((bob *∨ carol)*→) *∧ (bob*←))
   {- Channel: Bob -> Alice -}
   toAliceFromBobForCarol = mkStdout (((alice *∨ bob *∨ carol)*→) *∧ (bob*←))
   {- Channel: Alice -> Carol -}
   fromAliceToCarol  = mkStdin  (((alice *∨ carol)*→) *∧ (alice*←))
   {- Channel: Carol -> Alice -}
   toAliceFromCarol = mkStdout  (((alice *∨ carol)*→) *∧ (carol*←))
   (>>)   = (P.>>)
   (>>=)  = (P.>>=)
