{-# LANGUAGE TypeOperators, PostfixOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RebindableSyntax #-}
{-# OPTIONS_GHC -fplugin Flame.Solver #-}

import Prelude hiding ( return, (>>=), (>>)
                      , print, putStr, putStrLn, getLine)
import Text.Read
import qualified Prelude as P ( return, (>>=), (>>) )
import Data.List
import Data.Proxy
import Data.String
import Data.Maybe
import Flame.Runtime.Time
import Data.Time as T
import Data.ByteString.Char8 (ByteString, pack, unpack, empty)

import Flame.Runtime.Prelude
import Flame.Macaroons
import qualified Macaroons as M
import Flame.Runtime.Principals
import Flame.Principals
import Flame.IFC 
import Flame.Runtime.IO
import qualified System.IO as SIO
import Data.IORef as SIO
import Flame.Runtime.IFCRef as Ref
import Data.Functor.Identity

{- Static principals -}
alice = SName (Proxy :: Proxy "Alice")
type Alice = KName "Alice"

bob = SName (Proxy :: Proxy "Bob")
type Bob = KName "Bob"

carol = SName (Proxy :: Proxy "Carol")
type Carol = KName "Carol"

{- The macaroon key -}
aliceKey :: Lbl ((C (Alice ∨ Carol)) ∧ I Carol) ByteString
aliceKey = label "alice's secret"

aliceKeyName :: ByteString
aliceKeyName = "alice's key"

{- The macaroon key -}
bobKey :: Lbl ((C (Bob ∨ Carol)) ∧ I Carol) ByteString
bobKey = label "bob's secret"

bobKeyName :: ByteString
bobKeyName = "bob's key"

carolLoc = "loc://carol"

checkTime :: SPrin pc -> SPrin l -> String -> IFC IO pc l Bool
checkTime pc l caveat =
  if "time <= " `isPrefixOf` caveat then
    case (parseTimeM True defaultTimeLocale "%Y-%m-%d" $ drop 7 caveat) of
      Just when -> do
        now <- getCurrentTimex pc
        return $ (utctDay now) <= when
      Nothing -> protect False
  else protect False

issueMac :: SPrin p 
         -> Lbl (C p ∧ I Carol) Macaroon
         -> UTCTime
         -> FLAHandle (C p ∧ I Carol)
         -> IFC IO (C p ∧ I Carol) SU ()
issueMac p lroot deadline toP =
  do root <- liftx pc lroot
     let (mac1, MacaroonSuccess) = 
              addFirstPartyCaveat root (until deadline) 
         (mac2, MacaroonSuccess) = 
           addFirstPartyCaveat mac1 "op: update"
         (serialized, MacaroonSuccess) = 
           serialize mac2 MacaroonV1 in
       hPutStrLnx pc toP $ unpack serialized
  where until day = pack $ "time <= " 
                  ++ formatTime defaultTimeLocale 
                      "%Y-%m-%d" day
        pc = (p*→) *∧ (carol*←)

aliceUpdateBid :: FLAHandle (C (Alice ∨ Carol) ∧ I Alice)
          -> Lbl (C (Alice ∨ Carol) ∧ I Carol) ByteString
          -> IFCRef (C (Alice ∨ Carol) ∧ I Carol) Integer
          -> IFC IO (C (Alice ∨ Carol) ∧ I Carol) SU ()
aliceUpdateBid from_p cKey bidref =
  do (mac, err) <- inputMac
     if err /= MacaroonSuccess then do
       error "Could not deserialize macaroon."
     else do
       v <- verifierCreatex pc pc l
       satisfyExactx pc v "op: update"
       satisfyGeneralx pc v (checkTime pc l)
       (res, _) <- verifyx pc v mac cKey []
       if res then
          assume ((alice*←) ≽ (carol*←)) $ do 
            bidstr <- hGetLinex pc from_p
            writeIFCRefx pc bidref (read bidstr)
       else 
         error "Could not verify macaroon."
 where pc = ((alice *∨ carol)*→) *∧ (carol*←)
       l  = ((alice *∨ carol)*→) *∧ (carol*←)
       inputMac :: IFC IO (C (Alice ∨ Carol) ∧ I Carol)
                          (C (Alice ∨ Carol) ∧ I Carol)
                          (Macaroon, ReturnCode)
       inputMac = assume ((alice*←) ≽ (carol*←)) $ do
                   mac <- hGetLinex pc from_p
                   return $ deserialize . pack $ mac

bobUpdateBid :: FLAHandle (C (Bob ∨ Carol) ∧ I Bob)
          -> Lbl (C (Bob ∨ Carol) ∧ I Carol) ByteString
          -> IFCRef (C (Bob ∨ Carol) ∧ I Carol) Integer
          -> IFC IO (C (Bob ∨ Carol) ∧ I Carol) SU ()
bobUpdateBid from_p cKey bidref =
  do (mac, err) <- inputMac
     if err /= MacaroonSuccess then do
       error "Could not deserialize macaroon."
     else do
       v <- verifierCreatex pc pc l
       satisfyExactx pc v "op: update"
       satisfyGeneralx pc v (checkTime pc l)
       (res, _) <- verifyx pc v mac cKey []
       if res then
          assume ((bob*←) ≽ (carol*←)) $ do 
            bidstr <- hGetLinex pc from_p
            writeIFCRefx pc bidref (read bidstr)
       else 
         error "Could not verify macaroon."
 where pc = ((bob *∨ carol)*→) *∧ (carol*←)
       l  = ((bob *∨ carol)*→) *∧ (carol*←)
       inputMac :: IFC IO (C (Bob ∨ Carol) ∧ I Carol)
                          (C (Bob ∨ Carol) ∧ I Carol)
                          (Macaroon, ReturnCode)
       inputMac = assume ((bob*←) ≽ (carol*←)) $ do
                   mac <- hGetLinex pc from_p
                   return $ deserialize . pack $ mac

runAuction :: SPrin p
           -> SPrin q
           -> IFCRef (C (Alice ∨ Carol) ∧ I Carol) Integer
           -> IFCRef (C (Bob ∨ Carol) ∧ I Carol) Integer
           -> FLAHandle (C p ∧ I Carol)
           -> FLAHandle (C q ∧ I Carol)
           -> IFC IO Carol SU ()
runAuction p q lpbid lqbid to_p to_q =
  assume ((*∇) p ≽ (*∇) carol) $ 
   assume (p ≽ carol) $ do
  assume ((*∇) q ≽ (*∇) carol) $ 
   assume (q ≽ carol) $ do
     lpbid <- readIFCRefx carol lpbid
     lqbid <- readIFCRefx carol lqbid
     if lpbid > lqbid then do
       hPutStrLnx carol to_p ("Alice wins!")
       hPutStrLnx carol to_q ("Alice wins!")
     else if lpbid < lqbid then do
       hPutStrLnx carol to_p ("Bob wins!")
       hPutStrLnx carol to_q ("Bob wins!")
     else do
       hPutStrLnx carol to_p "It's a tie!"
       hPutStrLnx carol to_q "It's a tie!"

main :: IO (Lbl SU ())
main = do 
  {- create a protected reference to the bids -}
  aliceBidRef' <- runIFC $ newIFCRefx publicTrusted (((alice *∨ carol)*→) *∧ (carol*←)) 0
  bobBidRef'   <- runIFC $ newIFCRefx publicTrusted (((bob *∨ carol)*→) *∧ (carol*←)) 0
  {- the references themselves are public and trusted -}
  let aliceBidRef = unlabelPT aliceBidRef' in
    let bobBidRef   = unlabelPT bobBidRef' in do
     now      <- T.getCurrentTime
     let deadline = addUTCTime (realToFrac 60) now in do
      {- carol sends alice a macaroon -}
      runIFC $ issueMac alice aliceMac deadline toAliceFromCarol 
      {- carol sends bob a macaroon -}
      runIFC $ issueMac bob bobMac deadline toBobFromCarol 
      runIFC $ aliceUpdateBid fromAliceToCarol aliceKey aliceBidRef
      runIFC $ bobUpdateBid   fromBobToCarol   bobKey   bobBidRef
      {- TODO: omitted: wait until deadline passes -}
      runIFC $ runAuction alice bob aliceBidRef bobBidRef toAliceFromCarol toBobFromCarol
 where
  aliceMac :: Lbl (C Alice ∧ I Carol) Macaroon
  aliceMac = bind aliceKey $ \key ->
            case create carolLoc key aliceKeyName of
              (m, MacaroonSuccess) -> label m
              _ -> error "Error creating macaroon"
  bobMac :: Lbl (C Bob ∧ I Carol) Macaroon
  bobMac = bind bobKey $ \key ->
            case create carolLoc key bobKeyName of
              (m, MacaroonSuccess) -> label m
              _ -> error "Error creating macaroon"
  {- Channel: Carol -> Alice -}
  toAliceFromCarol  = mkStdout ((alice*→) *∧ (carol*←))
  {- Channel: Alice -> Carol -}
  fromAliceToCarol  = mkStdin  (((alice *∨ carol)*→) *∧ (alice*←))
  {- Channel: Carol -> Bob -}
  toBobFromCarol  = mkStdout ((bob*→) *∧ (carol*←))
  {- Channel: Bob -> Carol -}
  fromBobToCarol  = mkStdin  (((bob *∨ carol)*→) *∧ (bob*←))
  (>>)   = (P.>>)
  (>>=)  = (P.>>=)
