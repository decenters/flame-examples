{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE PostfixOperators #-}
{-# OPTIONS_GHC -fplugin Flame.Solver #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module MemoDBMacAuthServer where
-- ghc -e "MemoDBMacAuthServer.main" -package ghc MemoDBMacAuthServer.hs
-- Based on Servant.API.Experimental.Auth and
-- https://github.com/krdlab/examples/blob/master/haskell-servant-webapp/src/Main.hs
import Prelude hiding (id)
import Data.Hex

import GHC.TypeLits (KnownNat, KnownSymbol, natVal, symbolVal)
import Network.HTTP.Types.Status
import Network.HTTP.Types.Method
import Network.HTTP.Types.Header

import qualified Control.Monad.IO.Class as XXX (liftIO) 
import Data.Aeson (FromJSON(..), ToJSON(..), encode)
import qualified Data.Map as M
import Data.Text (Text)
import Data.Time (LocalTime(..), TimeZone(..), ZonedTime(..), hoursToTimeZone, midday, fromGregorian, utcToZonedTime, getCurrentTimeZone)
import GHC.Generics (Generic)
import Network.Wai (Application, Response)
import Network.Wai.Handler.Warp (run)
import System.IO as SIO
import Servant ((:>), (:<|>)(..), ReqBody, Capture, Get, Post, Delete, Proxy(..), Server, serve, ServantErr, HasServer(..), reflectMethod, ReflectMethod(..), serveDirectoryFileServer)
import           Servant.API.ContentTypes
import Servant.Docs (docs, ToCapture(..), DocCapture(..), ToSample(..), markdown, singleSample, HasDocs(..))
import Servant.Docs.Internal (ToAuthInfo(..), authInfo, DocAuthentication(..))
import Servant.Server.Internal
import Control.Lens ((|>), over)
import System.Environment (getArgs)

import Data.Maybe  (fromJust, fromMaybe)
import Data.Aeson                       (ToJSON)
import Data.ByteString                  (ByteString, unpack)
import Data.Map                         (Map, fromList)
import Data.Monoid                      ((<>))
import qualified Data.Map            as Map
import Data.Proxy                       (Proxy (Proxy))
import Data.Text                        (Text, pack)
import GHC.Generics                     (Generic)
import Network.Wai                      (Request, requestHeaders)
import Servant.API                      ((:<|>) ((:<|>)), (:>), BasicAuth,
                                          Get, JSON, Verb(..))
import Servant.API.BasicAuth            (BasicAuthData (BasicAuthData))
import Servant.API.Experimental.Auth    (AuthProtect)
import Servant                          (throwError)
import Servant.Server                   (BasicAuthCheck (BasicAuthCheck),
                                         BasicAuthResult( Authorized
                                                        , Unauthorized
                                                        ),
                                         Context ((:.), EmptyContext),
                                         err401, err403, errBody, Server,
                                         serveWithContext, Handler,
                                         ServerT, (:~>)(..), Application, serve, enter)
import Servant.Server.Experimental.Auth (AuthHandler, AuthServerData,
                                         mkAuthHandler)
import Servant.Server.Experimental.Auth()
import Control.Monad.Trans.Resource     (MonadResource (..), ResourceT, runResourceT)


import Flame.Macaroons
import Flame.Servant
import Flame.Servant.Server
import Flame.Servant.MacaroonAuth
import Debug.Trace

import Flame.Runtime.Principals
import Flame.IFC
import Flame.Principals
import Flame.Runtime.Time as T
import Control.Concurrent.STM hiding (atomically)
import Flame.Runtime.STM as F
import qualified Flame.Runtime.Prelude as F hiding (id)
import Flame.Runtime.Sealed
import Flame.Runtime.IO
import Data.String
import System.IO.Unsafe
import Control.Monad.Trans.Either (EitherT)

import Flame.TCB.Assume
import Flame.TCB.IFC (Lbl(..))
import qualified Data.ByteString.Char8 as C
import Data.ByteString.Lazy (toStrict)

-- JS stuff
import qualified Data.Text.IO as TextIO
import Servant.Foreign
import Servant.JS
import qualified Language.Javascript.JQuery
import Control.Lens hiding (use, Context)

{- Auth specific -}
type Alice = KName "Alice"
alice = Name "Alice"

type Bob = KName "Bob"
bob = Name "Bob"

type Carol = KName "Carol"
carol = Name "Carol"

-- | We need to specify the data returned after authentication
type instance AuthServerData (AuthProtect "macaroon-auth") = FLAC IO MemoAuthBound MemoAuthBound MemoAuth

{- memo specific -}
data Memo = Memo
    { id :: Int
    , text :: Text
    , createdAt :: UTCTime
    }
    deriving (Show, Generic)

instance ToJSON Memo

data ReqMemo = ReqMemo { memo :: Text } deriving (Show, Generic)

instance FromJSON ReqMemo
instance ToJSON ReqMemo

type MemoAPI =
         "memos" :> AuthProtect "macaroon-auth"
          :> EnforceFLA MemoClient MemoClient (Get '[JSON] [Memo]) 
    :<|> "memos" :> AuthProtect "macaroon-auth"
          :> ReqBody '[JSON] ReqMemo
          :> EnforceFLA MemoClient MemoClient (Post '[JSON] Memo) 
    :<|> "memos" :> AuthProtect "macaroon-auth"
         :> Capture "id" Int
         :>  EnforceFLA MemoClient MemoClient (Delete '[JSON] ())

memoAPI :: Proxy MemoAPI
memoAPI = Proxy

type MemoAPI_Raw = MemoAPI :<|> Raw
memoAPI_Raw :: Proxy MemoAPI_Raw
memoAPI_Raw = Proxy

type MemoAuth = IFCAuth MemoAPI

type MemoKeyStore = Lbl (I MemoServer) (KeyDBVar MemoAPI)

type MemoStore = Lbl (I MemoServer) MemoDB

type MemoDB = IFCTVar (I MemoServer) (M.Map Prin (SealedType IFCTVar (Int, M.Map Int Memo)))

-- | The context that will be made available to request handlers. We supply the
-- "macaroon-auth"-tagged request handler defined above, so that the 'HasServer' instance
-- of 'AuthProtect' can extract the handler and run it on the request.
genAuthServerContext :: MemoKeyStore -> Context (AuthHandler Request (FLAC IO MemoAuthBound MemoAuthBound MemoAuth) ': '[])
genAuthServerContext keyDB = (macaroonAuthHandler keyDB memoMkVerifier) :. EmptyContext


memoMkVerifier :: Lbl MemoClient Request -> FLAC IO MemoAuthBound MemoAuthBound (IFCVerifier MemoAuthBound MemoAuthBound)
memoMkVerifier req = verifierCreate (authBoundTy memoAPI) (authBoundTy memoAPI)

apiJS :: Text
apiJS = jsForAPI memoAPI jquery

instance (HasForeignType lang ftype Text, HasForeign lang ftype api)
  => HasForeign lang ftype (AuthProtect "macaroon-auth" :> api) where
  type Foreign ftype (AuthProtect "macaroon-auth" :> api) = Foreign ftype api

  foreignFor lang Proxy Proxy req =
    foreignFor lang Proxy subP $ req & reqHeaders <>~ [HeaderArg arg]
    where
      hname = "servant-auth-macaroon"
      arg   = Arg
        { _argName = PathSegment hname
        , _argType  = typeFor lang (Proxy :: Proxy ftype) (Proxy :: Proxy Text) }
      subP  = Proxy :: Proxy api

writeJS :: IO ()
writeJS = do
  TextIO.writeFile "html/api.js" apiJS
  jq <- TextIO.readFile =<< Language.Javascript.JQuery.file
  TextIO.writeFile "html/jq.js" jq

main :: IO ()
main = do
    let (Right aliceMac) =  create "alice" "1secret" "key1"
    --let (Right aliceCavMac) = addFirstPartyCaveat aliceMac
    --        (toStrict . encode $ SVC "caveat author pc l memoDB = protect True" [] bob "")
    let (Right macstr) = serialize aliceMac MacaroonV1
    putStrLn ("alice's macaroon:" ++ (C.unpack macstr)) 
    let Right mac = deserialize macstr
    let Right s = inspect mac
    putStrLn s
    writeJS >> runApp
    
runApp :: IO ()
runApp = do 
  init <- runFLAC $ use initialMemoStore $ \store -> newIFCTVarIO store
  keyDBvar <- runFLAC $ use initialKeyDB $ \keyDB-> newIFCTVarIO keyDB
  run 8080 $ app keyDBvar init

  where
    initialKeyDB :: FLAC IO (I MemoServer) (I MemoServer) (KeyDB MemoAPI)
    initialKeyDB = protect $ fromList [ ("key1", (alice, label "1secret"))
                                      , ("key2", (bob,   label "2secret"))
                                      , ("key3", (carol, label "3secret"))
                                      ]

    newMemoDB :: DPrin p
              -> FLAC IO (I MemoServer) (I MemoServer)
                  (IFCTVar p (Int, M.Map Int Memo))
    newMemoDB p = newIFCTVarIO (0, M.empty)

    initialMemoStore :: FLAC IO (I MemoServer) (I MemoServer)
                         (M.Map Prin (SealedType IFCTVar (Int, M.Map Int Memo)))
    initialMemoStore =
      withPrin alice $ \alice' -> withPrin bob $ \bob' -> withPrin carol $ \carol' -> 
        use (newMemoDB alice') $ \aliceDB ->
        use (newMemoDB bob')   $ \bobDB ->
        use (newMemoDB carol') $ \carolDB ->
        protect $ fromList [ (alice, sealType alice' aliceDB)
                           , (bob,   sealType bob'   bobDB)
                           , (carol, sealType carol' carolDB)
                           ]

app :: MemoKeyStore -> MemoStore -> Application
app keyDBvar s = serveWithContext memoAPI_Raw (genAuthServerContext keyDBvar) $ (server s :<|> serveDirectoryFileServer "html")

type MemoServer = KTop
type MemoClient = KName "MemoClient"
type MemoAuthBound = (C MemoClient) ∧ (I MemoServer)

{- common defs -}
instance IFCApp MemoAPI where
  type AppServer MemoAPI = MemoServer
  type Client MemoAPI = MemoClient
  type AuthBound MemoAPI = MemoAuthBound
  
  appServerPrin     = const $ Top
  appServerTy       = const $ STop

  currentClientPrin = const $ Name "MemoClient"
  currentClientTy   = const $ SName (Proxy :: Proxy "MemoClient")

  authBoundPrin api = Conj (Conf $ currentClientPrin api) (Integ $ appServerPrin api)
  authBoundTy   api = ((currentClientTy api)*->) *∧ ((appServerTy api)*<-)

server :: MemoStore -> Server MemoAPI
server s = getMemosH :<|> postMemoH :<|> deleteMemoH 
  where
    apiMemoClient = currentClient memoAPI
    apiMemoServer = appServer memoAPI
    mkSig client = (dyn client, dyn client)
    deleteMemoH :: FLAC IO MemoAuthBound MemoAuthBound MemoAuth
                 -> Int
                 -> FLAC Handler MemoClient MemoClient ()
-- Example bug: MemoClient labeled info in macaroon could leak via deletion of public memo
--                 -> FLAC Handler (I MemoClient) MemoClient ()
    deleteMemoH auth i = mkHandler memoAPI auth mkSig apiMemoClient apiMemoClient $
       \(client :: DPrin client) pc' l' -> do
            Equiv <- pc' `eq` client
            Equiv <- l' `eq` client
            return (Equiv, Equiv, reprotect $ ebind s $ \db -> deleteMemo client db i)

    getMemosH :: FLAC IO MemoAuthBound MemoAuthBound MemoAuth
              -> FLAC Handler MemoClient MemoClient [Memo]
    getMemosH auth = mkHandler memoAPI auth mkSig apiMemoClient apiMemoClient $
       \(client :: DPrin client) pc' l' -> do
            Equiv <- pc' `eq` client
            Equiv <- l' `eq` client
            return (Equiv, Equiv, reprotect $ ebind s $ \db -> getMemos client db)

    postMemoH :: FLAC IO MemoAuthBound MemoAuthBound MemoAuth
              -> ReqMemo
              -> FLAC Handler MemoClient MemoClient Memo
    postMemoH auth r = mkHandler memoAPI auth mkSig apiMemoClient apiMemoClient $
       \(client :: DPrin client) pc' l' -> do
            Equiv <- pc' `eq` client
            Equiv <- l' `eq` client
            return (Equiv, Equiv, reprotect $ ebind s $ \db -> postMemo client db r)


withMemoDB :: forall client pc l b .
             DPrin client
             -> MemoDB
             -> (forall client'. (client === client') =>
                                 DPrin client'
                              -> IFCTVar client' (Int, (M.Map Int Memo))
                              -> FLAC STM pc l b)
             -> FLAC STM pc l b
withMemoDB client db f =
  use (readIFCTVar db) $ \db' -> 
  fromJust $ do -- TODO: handle Nothing case!
   entry <- M.lookup (dyn client) db'
   unsealTypeWith @client @IFCTVar
     @(Int, Map Int Memo) @(FLAC STM pc l b)
     client entry f

getMemos :: forall client. DPrin client
         -> MemoDB
         -> FLAC IO client client [Memo]
getMemos client db = atomically $
    withMemoDB client db $ \(client':: DPrin client') db' ->
    use (readIFCTVar db') $ \(_, m) ->
     protect $ M.elems m :: FLAC STM client client [Memo]

postMemo :: forall client. DPrin client
         -> MemoDB
         -> ReqMemo
         -> FLAC IO client client Memo
postMemo client db req =
   use getCurrentTime $ \time -> atomically $
   withMemoDB client db $ \(client':: DPrin client') db' ->
   use (readIFCTVar db') $ \(c, sm) ->
     let m = Memo (c + 1) (memo req) time
         entry = (id m, M.insert (id m) m sm)
     in
       apply (writeIFCTVar db' entry) $ \m' ->
       protect m

deleteMemo :: DPrin client
           -> MemoDB
           -> Int
           -> FLAC IO client client ()
deleteMemo client db i = atomically $
  withMemoDB client db $ \(client':: DPrin l) db' ->
  modifyIFCTVar db' $ \(c, sm) ->
  (c, M.delete i sm)

-- NOTE: ↓ for documentation
instance ToCapture (Capture "id" Int) where
    toCapture _ = DocCapture "id" "memo id"

instance ToSample Memo where
    toSamples _ = singleSample sampleMemo

instance ToSample [Memo] where
    toSamples _ = singleSample [sampleMemo]

instance ToSample ReqMemo where
    toSamples _ = singleSample $ ReqMemo "Try haskell-servant."

instance ToSample (FLAC IO MemoClient MemoClient Memo) where
    toSamples _ = singleSample $ protect sampleMemo

instance ToSample (FLAC IO MemoClient MemoClient [Memo]) where
    toSamples _ = singleSample $ protect [sampleMemo]

instance ToSample (FLAC IO MemoClient MemoClient ()) where
    toSamples _ = singleSample $ protect ()

sampleMemo :: Memo
sampleMemo = Memo
    { id = 5
    , text = "Try haskell-servant."
    , createdAt = UTCTime (fromGregorian 2014 12 31) (secondsToDiffTime 0)
    }

instance (ToAuthInfo (AuthProtect "macaroon-auth"), HasDocs api) => HasDocs (AuthProtect "macaroon-auth" :> api) where
  docsFor Proxy (endpoint, action) =
    docsFor (Proxy :: Proxy api) (endpoint, action')
      where
        authProxy = Proxy :: Proxy (AuthProtect "macaroon-auth")
        action' = over authInfo (|> toAuthInfo authProxy) action

instance ToAuthInfo (AuthProtect "macaroon-auth") where
  toAuthInfo p = DocAuthentication "servant-auth-macaroon" "A key in the principal database"
